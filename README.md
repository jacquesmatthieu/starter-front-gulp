## Getting Started

- Install package: `npm install`
- Install bower: `bower install`
- Run `gulp serve` to preview and watch for changes
- Run `bower install --save <package>` to install frontend dependencies
- Run `gulp serve:test` to run the tests in the browser
- Run `gulp` to build your webapp for production
- Run `gulp serve:dist` to preview the production build


## See your webapp

-  `ip_server:9000` to preview the production build


## See Brownsync panel

-  `ip_server:3001` to preview the production build

## Web app tree

```
app/
├── font/
├── images/
├── pages/
├── script/
	└── main.js
├── style/
	└── base
		└── _default.scss
		└── _var.scss
	└── layout
		└── _footer.scss
		└── _header.scss
		└── _nav.scss
		└── _sidebar.scss
	└── mixins
		└── _mixin-typography.scss
		└── _position.scss
	└── module
		└── _form.scss
		└── _normalize.scss
		└── _wysiwyg.scss
	main.scss
├── templates/
	└── partials
		└── index.nunjucks
	layout.nunjucks
├── dist/
├── test/

```